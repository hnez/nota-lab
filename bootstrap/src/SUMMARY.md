# NoT A Lab

[Introduction](introduction.md)

- [Software Setup](chapter_1/overview.md)
    - [Generating the Instructions](chapter_1/mdbook.md)
    - [Disk Images](chapter_1/vmdb2.md)
    - [Flash the Microcontroller Boards](chapter_1/flash_nodemcu.md)
- [Hardware Setup](chapter_2/overview.md)
- [Behind the Scenes](chapter_3/overview.md)
    - [Network Configuration](chapter_3/network.md)
    - [Certificate Authority](chapter_3/certificates.md)
    - [Cloud Server](chapter_3/cloud_server.md)
    - [Recording screencasts](chapter_3/screencasts.md)
