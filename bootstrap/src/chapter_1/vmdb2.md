Generating disk images
======================

The operating system images for the Raspberry Pis
and the cloud application are automatically
generated using [vmdb2][www_vmdb2].
To install it and other build requirements run:

```bash
$ sudo apt install make vmdb2 qemu-system qemu-user-static openssl debian-keyring
```

We will be using the official Debian repositories instead
of Raspbian on the Raspberry Pis, this means that first-generation
Raspberry Pis, which use an older CPU, will not work with
the generated image file.

The cloud image is also based on Debian and is expected
to run in a virtual machine in the same network as
the Raspberry Pis, or at least be reachable by them.

To generate the image file change into the directory containing
the image descriptions and run `make` to generate the image
files.

```bash
$ cd ~/Downloads/nota-lab/disk_images
$ make
```

_Note:_ On a computer with a spinning harddrive
(e.g.: not an SSD) this process can take hours,
and you will have to check then and now if
you have to enter your password.

You may need to enter a password multiple times as parts of the build
process use `sudo` to gain root priviliges.

While the Raspberry Pi image is building you can follow the status
using the following command:

```bash
$ sudo tail -n 1000 -f ~/Downloads/nota-lab/disk_images/rpi_student.img.log
```

While the Cloud image is building you can follow the status using
this command instead:

```bash
$ sudo tail -n 1000 -f ~/Downloads/nota-lab/disk_images/vm_server.img.log
```

If everything goes well you should, after some minutes,
end up with a file called `rpi_student.img` and another
file called `vm_server.img`.

Server setup
------------

The server setup should be performed first as the
Raspberry Pi setup instructions expect the server
to be available.

The cloud image can be set as the harddisk image in your
virtualization environment of choice.
You may also have some luck with writing the
image to an USB drive like you would write the
Raspberry Pi image to an SD card and boot
from that drive.

To simplify the setup the network should be setup to
be bridged to the ethernet interface of the server.

To test the image without the necessary
network setup you can also try running:

```bash
$ make run_server
```

Server first boot
-----------------

On the first start the image will ask some
questions and setup a `tutor` account with
a provided password.

Raspberry Pi setup
------------------

You can flash the Raspberry Pi images on an SD-Card with a `dd`
command like the following:

```bash
$ sudo dd if=~/Downloads/nota-lab/disk_images/rpi_student.img bs=8M of=/dev/sd_
```

Where you replace `/dev/sd_` by the drive name of your SD-Card
as discovered by e.g. `dmesg -w` or `blkid`.

The Raspberry Pi file will be 7Gb large, so that it will fit safely
on an 8Gb SD-Card. If you need more storage space you may
use `cfdisk` and `resize2fs` on the running Raspberry Pi,
if you need the image to fit on smaller cards you can change
the first lines of `rpi_student.vmdb` to generate a different
image size.

Raspberry Pi first boot
-----------------------

Upon first boot the Raspberry Pis will ask
some questions regarding the keyboard layout and
timezone.

For most questions you can go with whatever
the example says, note however that the
keyboard layout is not set before the first reboot,
so you will have to deal with an american keyboard layout
where the `/` character is left of the right shift key.

The ids you give the setup script have to be
unique in the lab as the hostname and
WiFi SSID will be derived from them.
You can however use characters or numbers or
whatever suits you best.

When you are done answering questions
the Pi will reboot and you can log in using
the `student` account and the password you provided.

When asked about the xfce4-panel configuration answer
"Use defaults".

Finally we have to work around a quirk in the firefox
version used that makes it difficult for image builders
to install custom certificates and easy for enduser, which
is <s>totally stupid</s> their design decision.

With the cloud server running in the same network
perform the following steps in the webbrowser to
install the certificate:

<video style="width: 100%" controls>
  <source src="assets/firefox_cert.mp4" type="video/mp4">
  Install the nota certificate in firefox
</video>

You can also perform this step on other computers
in the network in order to access the lab materials
from them.

Maintainance
============

You should run the image creation process each year
before the lab takes place to stay on top of security
updates.

Even if you do not generate a new image you should
at least re-flash the image to the SD-Cards
to make sure the students work in a
fresh environment.

[www_vmdb2]: http://git.liw.fi/vmdb2
