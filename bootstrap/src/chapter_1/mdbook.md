Generating the instructions
===========================

The first task will be to generate a more
readable form of these setup instructions
and of the instructions for the students.

To generate these [mdBook][www_mdbook] is used.
To install mdBook and other build requirements
you have to follow these steps:

1. Install requirements from the debian reposiory:
   ```bash
   $ sudo apt install git make cargo
   ```
2. Install mdBook using cargo:
    ```bash
    $ cargo install mdbook
    ```
3. Add cargo's binary directory to your `PATH`:
    ```bash
    $ echo 'PATH=${PATH}:${HOME}/.cargo/bin' >> ~/.bashrc`
    ```
4. Open a new terminal and check if everything worked
   by running:
   ```bash
   $ mdbook
   ```

If none of the commands above threw any errors
you should now be ready to download and compile
the lab instructions using the following steps:

1. Clone the repository:
   ```bash
   $ cd Downloads
   $ git clone "https://gitlab.com/nota-lab/nota-lab"
   ```
2. Build the bootstrap instructions:
   ```bash
   $ cd nota-lab/bootstrap/
   $ make
   ```
3. Build the lab instructions:
   ```bash
   $ cd ../instructions/
   $ make
   ```
4. The generated instructions can be found in
   the `bootstrap/book` and `instructions/book`
   folders and can be viewed using a web browser:
   ```bash
   $ cd ../bootstrap/
   $ firefox book/index.html
   ```

You can now change the source files in the `bootstrap/src`
and `instructions/src` folders as you see fit and re-run
`make` to generate an HTML version.

[www_mdbook]: https://github.com/rust-lang-nursery/mdBook/
