Flashing NodeMCU
================

Building
--------

The Microcontroller boards need to be manually
flashed with the NodeMCU firmware in order to
run the Lua programs developed by the students.

Luckily we do not need to set up the complex
ESP8266 SDK and can instead use the
[Online builder][www_builder] provided by
the NodeMCU devs.

Go to the builder website, enter a valid
mail address and select the following
modules to include:

- DHT
- file
- GPIO
- MQTT
- net
- node
- PWM
- timer
- UART
- WiFi

Finally hit "Build" and wait for the firmware
file to arrive via E-Mail.

Flashing
--------

In the E-Mail you will receive a link to a
float and an integer version of the firmware.
We will be using the float version.

Download the firmware and use esptool
(which comes pre-installed on the Raspberry Pi
images) to upload it to the microcontroller boards:

```bash
$ esptool.py --port /dev/ttyUSB0  write_flash 0 nodemcu-master-…-float.bin
esptool.py v2.5.0
Serial port /dev/ttyUSB0
Connecting....
Detecting chip type... ESP8266
Chip is ESP8266EX
Features: WiFi
MAC: 12:34:56:78:9A:CD
Uploading stub...
Running stub...
Stub running...
Configuring flash size...
Auto-detected Flash size: 4MB
Flash params set to 0x0240
Compressed 430704 bytes to 277188...
Wrote 430704 bytes (277188 compressed) at 0x00000000 in 24.4 seconds (effective 141.2 kbit/s)...
Hash of data verified.

Leaving...
Hard resetting via RTS pin...
```

_Note:_ Depending on the source of your esptool installation
the command name may be `esptool.py` or `esptool`.

This flashing has to be performed once per board.

Maintainance
============

You should run the image creation process each year
before the lab takes place to stay on top of security
updates.

Even if you do not generate a new image you should
at least re-flash the image to the microcontrollers
to make sure the students work in a
fresh environment.

[www_builder]: https://nodemcu-build.com/
