# Introduction

This is a guide for future tutors of
the NoT A Lab that will
guide you through the necessary hardware
and software setups.

These instructions assume that you have
a running installation of Debian Buster
or new which, at the time of writing,
is the current Debian testing version.

If you have such a system running or are
willing to adapt you can continue to
[generating the lab instructions](./chapter_1/mdbook.html).
