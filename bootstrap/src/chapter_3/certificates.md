Certificate Authority
=====================

A design goal of the cloud unit is for the
students to learn that communication over
the public internet may not happen unencrypted.

This is why the fake cloud environment also uses
encrypted HTTPS.

In order to enable HTTPS we need certificates signed
by a certificate authority trusted by all the
participants in the network.
Acquiring certificates signed by a real CA would
complicate the lab setup so a fake CA is created
during the disk image build process.

This is all done by the `Makefile` in
`disk_images/certificates/` that sets up the
CA, creates a key for the cloud server to use
and creates a signed certificate for that key.

If you decide to run the server with a custom domain
name you should add that domain name to
the `disk_images/certificates/nota-cloud.conf` file.

FAQ
===

- The certificates appear invalid:
    - Check that the system time is correct and that the devices can reach an NTP server.
    - Make sure the virtual machine and Raspberry Pi images ware built on the same
      host and in one go.
