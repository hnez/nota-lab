Network configuration
=====================

Address configuration
=====================

The student Raspberry Pis and the virtual
machine try getting an IPv4 address on their
ethernet adapter via DHCP.

The simplest setup is to connect them all to
a simpler consumer router that hands out
IP addresses from a pool.

Each Raspberry Pi will, in addition, run an
isolated network on the WiFi interface
on which it gives out IP addresses using DHCP.

Clients connected to that network can
not access the internet, the other Raspberry Pis,
or the cloud server.

---

The network is condigured using `systemd-networkd`
in the configuration files in `/etc/system/network`.

The WiFi setup is done using `hostapd`.
The WiFi interface can not be used to connect to
wireless networks.

---

The old `ifconfig` line of tools are deprecated,
use for example `ip addr` to show the IP addresses
assigned to you network interfaces.

DNS
===

The Raspberry Pis and the virtual machine
get assigned their DNS server to use using DHCP.

It is not assumed that custom domain names can
be added to the zones of that DNS server.
In order to get human-readable names
for e.g. the `nota-cloud` virtual machine
LLMNR is used, which sends DNS requests to
the whole subnet using multicast packets
and the corresponding host answers with its
IP address.

This will not work if the virtual machine
and the Pis are not in the same multicast
domain (≈ separated by a router).
The you will have to configure DNS to
resolve the domain name of the cloud server
and generate an appropriate server certificate.

Internet Access
===============

Also the Lab is mostly self contained there
are reasons to give internet access to the
Raspberry Pis and virtual machine, namely:

- The Raspberry Pis do not contain a real time clock,
  they need access to an NTP server to get the correct time.
  The server certificates can appear invalid when the
  the time is wrong.
- The students should be able to look up programming
  ressources while working.
