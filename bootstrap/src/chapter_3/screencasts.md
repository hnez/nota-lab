Recording Screencasts
=====================

The process of recording screencasts can
not be performed on a Raspberry Pi and must
instead be performed on a powerful computer
as the video encoding slows the Pi to much
for fluent screencasts.

Getting a fresh browser instance
--------------------------------

For screencasts you want a browser
session that is fresh as-installed.

The command below sets the language to be
english and uses a newly generated browser
profile.

```bash
$ LANG= firefox --new-instance --profile $(mktemp -d)
```

Cathing the window
------------------

The command below starts recording the screen with
the correct size for screencasts and shows it in
a new window.

Adjust the window you want to film to fit.

Adjust the `+2,28` to catch another part of the screen.

```bash
$ ffplay -video_size 750x480 -framerate 30 -f x11grab -i :0.0+2,28
```

Starting the recording
----------------------

The command below will record the screen into the file
`screencast.mp4`. Hit Ctrl+C when you are done.

```bash
$ ffmpeg -video_size 750x480 -framerate 30 -f x11grab -i :0.0+2,28 -an -vcodec libx264 -pix_fmt yuv420p -profile:v baseline -level 3 screencast.mp4
```

Extract still images
--------------------

The command takes a screenshot 16.1 seconds into
`screencast.mp4` and saves it as `still_image.png`.

```bash
$ ffmpeg -ss 16.1 -i screencast.mp4 -vframes 1 still_image.png
```
