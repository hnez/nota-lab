Cloud Server
============

Web Server
----------

Running on the virtual machine is an
instance of nginx that serves static files
and acts as reverse proxy.

When connected to on port 80, using unencrypted
HTTP, it serves the files in `/var/www/nota-nossl`.

When connected to on port 443, using encrypted
HTTPS, it serves the files in `/var/www/nota`
and will proxy requests to URLs below `https://nota-cloud/api/`
to the cloud API server.

This server is configured using the files in `/etc/nginx`
and controlled using `$ sudo systemctl restart nginx`.

API Server
----------

The API server serves the interactive part of
the cloud app, it runs with the user id of
the tutor user.

It is written in Python 3, using Bottle to
perform the request routing and gevents
to realize the event streaming.
Gunicorn is used to deploy the server.

This server is controlled using
`$ systemctl --user restart nota_api`.

---

The server expects data to be `POST`ed
to the `/api/log/<sensor>` endpoints,
where `<sensor>` is replaced by an arbitrary
name for the sensor.

The request Body has to be JSON encoded
and contain a "value" key with a float value
(`{ "value" : 10.0 }`).

Such a request can be created like this,
using [HTTPie][www_httpie]:

```bash
http POST https://nota-cloud/api/log/nota_2 value:=-2.0
```

The other important endpoint is `/api/stream`,
which sends a history of the most recent sensor
values and newly ariving ones as
server sent events.

You can watch this stream using the command
below:

```bash
curl https://nota-cloud/api/stream
```

Web App
=======

The web app subscribes to the `/api/stream`
using `EventSource` and plots a new point
on a line diagram whenever an event is received
using [Chart.js][www_chartjs].

<video style="width: 100%" controls>
  <source src="assets/cloud_app.mp4" type="video/mp4">
  Demo of the cloud application
</video>

[www_httpie]: https://httpie.org/
[www_chartjs]: https://www.chartjs.org/
