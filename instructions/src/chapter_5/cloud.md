Cloud Scenario
==============

Finally we want to also receive data
from the cloud, to do so we will use
HTTP `GET` requests.

We will create a new Node-RED flow
that periodically sends request
to the server and acts on the received
data.

<video style="width: 100%" controls>
  <source src="assets/nodered_get.mp4" type="video/mp4">
  GET requests in Node-RED
</video>

The video above uses an inject node
that sends an empty message every five
seconds to trigger a request to the server.

The response is then formatted using
function block containing the following code:

```javascript
var values = msg.payload['values'];
var last = values[values.length - 1];
var value = last['value'];

var out = { 'payload' : value };

return out;
```

_Task:_ Replicate the flow setup shown in the video.
Replace the endpoint configured in the http request node
by the one corresponding to your device name.

_Task:_ By reading the sourcecode and experimenting
find out what the flow does and what the output
of the "Formatter" function node is.

_Task:_ Control a microcontroller based on the output
of the "Formatter" block. E.g. turn on a light after a
certain number of button presses.

Group phase
===========

_Step 1:_ Choose a sensor you would like to use and,
using the microcontroller, MQTT, Node-RED
and HTTP build a complete chain to send sensor
readings to the cloud.

_Step 2:_ Fetch sensor readings _from another group_
from the cloud and control an actuator of
your liking based on the values you get.

_Example:_ One group builds a thermometer
that publishes temperatures to the cloud.
You take the readings from that sensor and,
once it gets too hot, use a servo motor
and a piece of paper to fan air.
