HTTP
===========

HTTP is an internet protocol on top of
TCP that was initially designed to transfer
websites over the internet.

Nowadays HTTP has become an ubiquitous protocol
used in a lot of applications like,
as we will see, IoT scenarios.

Before version 2.0 HTTP was a simple text
based protocol, so simple in fact that
will be manually sending an HTTP request to
a server using `netcat`.

Start `netcat` on the commandline and connect
to an HTTP server using the following command
(port 80 is the default port for unencrypted HTTP):

```bash
[user@computer ~]$ nc nota-cloud 80
```

Now enter the following text, making sure
to press the enter key whenever you see the
`⏎` symbol:

```
GET / HTTP/1.1⏎
Host: nota-cloud⏎
⏎
```

The output should now look something
like the following:

```
[user@computer ~]$ nc nota-cloud 80
GET / HTTP/1.1
Host: nota-cloud

HTTP/1.1 200 OK
Server: nginx/1.10.3
Date: Mon, 01 Jan 2018 00:00:01 GMT
Content-Type: text/html
Content-Length: 986
Last-Modified: Mon, 01 Jan 2018 00:00:00 GMT
Connection: keep-alive
ETag: "deadbeef-123"
Accept-Ranges: bytes

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <title>Unencrypted Nota Server content</title>
</head>
 <body>
  <h1>Unencrypted NoTA Server content</h1>
  <ul>
    <li><a href="https://nota-cloud/">Encrypted version</a></li>
    <li><a href="/nota-ca.crt">Certificate for encrypted page</a></li>
  </ul>
 </body>
</html>
```

In this output we can see most things we have
to know about HTTP.

Request Header
--------------

The request header is the part we sent to the server:

<pre><div class="buttons" style="display:none"></div><div class="hljs" style="font-size: 0.875em;"><span style="color:#75507b">GET</span> <span style="color:#f57900">/</span> HTTP/1.1
<span style="color:#3465a4">Host: nota-cloud</span></div></pre>

It contains:

- The <span style="color:#75507b">request method</span>, in this case we want to `GET`
  a ressource from the server, we could also `POST` ressources to the server or `DELETE`
  ressources if the server lets us.
- The <span style="color:#f57900">ressource path</span>, in this case we want to `GET`
  the index `/`, we could also request other ressources like `ressource.html`.
- The <span style="color:#3465a4">request header parameters</span>, in this case just
  the `Host` header that tells the server which host we expect to talk to.

Request Body
------------

When `POST`ing or `PUT`ing ressources to the server
we want to include actual data in our request.
This would be sent after the request headers.

In this case we did not send any data to the server.

Response Header
---------------

The response header is the first part of the
transfer sent by the server:

<pre><div class="buttons" style="display:none"></div><div class="hljs" style="font-size: 0.875em;">HTTP/1.1 <span style="color:#f57900">200 OK</span>
<span style="color:#3465a4">Server: nginx/1.10.3
Date: Mon, 01 Jan 2018 00:00:01 GMT
Content-Type: text/html
Content-Length: 986
Last-Modified: Mon, 01 Jan 2018 00:00:00 GMT
Connection: keep-alive
ETag: "deadbeef-123"
Accept-Ranges: bytes</span></div></pre>

The response header contains:

- The <span style="color:#f57900">status code</span>, in this case everything went
  well and we got a `200 OK` code, other common codes are `404 Not Found` or `500 Internal Server Error`.
- The <span style="color:#3465a4">response header parameters</span>, in this case
  for example the `Server` parameter that tells us that the server software is called `nginx`.

Response Body
-------------

Everything following the response headers is
part of the requested ressource, in this
case a HTML page intened to be rendered by
a web browser, containing links to other
ressources.

HTTPS
=====

Instead of bare TCP HTTP can also be
used with an encrypted TLS layer on top
of TCP, this is secure HTTP or HTTPS.

Using HTTPS instead of HTTP makes sure
that no unauthorized user can impersonate
the server and that the content of the transfer
can not be intercepted by third parties.

Nowadays unencrypted communication like HTTP over the public
internet is only used in legacy setups and is highly discouraged.

__Do not run unencrypted services over the public internet!__

__Do not run unencrypted services over the public internet!__

__Do not run unencrypted services over the public internet!__
