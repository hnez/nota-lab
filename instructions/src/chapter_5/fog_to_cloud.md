Fog to Cloud
============

In this subchapter we will link out Node-RED
based Fog setup to an existing cloud application
using HTTPS, REST and JSON.

First we will generate a Node-RED flow that
produces appropriately formatted JSON objects
and then we will send these fragments to the
cloud application.

Generating Data
---------------

First we will need to generate data to send
to the cloud application.

Task: Based on the "Fog Scenario" subchapter
write a function block that behaves like in
the video below. The node should output a number
that increments whenever a message with
the payload "pressed" is received.

<video style="width: 100%" controls>
  <source src="assets/nodered_upcounter.mp4" type="video/mp4">
  Counting node
</video>


Generating JSON
---------------

Next we will want a node that formats
the data in a format suitable for the
cloud application.

<video style="width: 100%" controls>
  <source src="assets/nodered_formatter.mp4" type="video/mp4">
  JSON Formatter
</video>

The following code, when placed in a function
block, does just that:

```javascript
var obj = {'value' : msg.payload};
var out = {'payload' : obj};

return out;
```

The output is of the format `{'value': 1}`,
ready to be sent to the cloud application.

Sending HTTP Requests
---------------------

The video below shows you how to setup Node-RED to send
an HTTP `POST` request to the `https://nota-cloud/api/log/nota-1`
endpoint whenever a button press event is received.

<video style="width: 100%" controls>
  <source src="assets/nodered_http.mp4" type="video/mp4">
  Sending HTTP requests with Node-RED
</video>

_Task:_ Add an HTTP request block to your flow.
Set the endpoint address to the one
corresponding to you device name.
