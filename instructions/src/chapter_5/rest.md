REST
====

In this lab we will be using REST over HTTPS to
send data to our cloud server.

In REST we use the appropriate HTTP method
to transfer state from and to the server.

The most important methods are:

- `GET` to get ressources from the server.
- `DELETE` to delete a ressource on the server.
- `POST` to create a new ressource on the server.
- `PUT` to replace a ressource with a new one.

---

We will now manually use some of these methods to
exchange information with the cloud server.

But first we will find the name of your
device as we will use it to identify us later
on. Execute the command below and remember its
output, it will be something like `nota-1` or `nota-2`.

```bash
[user@computer ~]$ uname -n
nota-x
```

Now open the [cloud application][nota_cloud]
in a new tab, when you successfully send data
later on you should see it appear there
(although you may have to refresh the page).

POST
----

First we will send a new ressource to the server
using the `POST` method.

Replace the `nota-x` part in the URL below by the
name of your device, as found out before.

```bash
[user@computer ~]$ http -v POST https://nota-cloud/api/log/nota-x value:=1
```

_Task:_ Execute the command multiple times with different values
and observe the cloud application.

GET
---

Next we want to get back the information sent to the
server using the `GET` method.

First we request a list of device names that sent
data to the server:

```bash
[user@computer ~]$  http -v GET https://nota-cloud/api/log
GET /api/log HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: nota-cloud
User-Agent: HTTPie/0.9.8



HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 40
Content-Type: application/json
Date: Mon, 01 Jan 2018 00:00:00 GMT
Server: nginx/1.10.3
Strict-Transport-Security: max-age=15768000

{
    "keys": [
        "nota-1",
        "nota-2",
        "nota-3"
    ]
}
```

We can see that the `nota-1`, `nota-2` and
`nota-3` ressources exist on the server.

Next we can request one of these ressources
from the server:

```
[user@computer ~]$ http -v GET https://nota-cloud/api/log/nota-1
GET /api/log/nota-1 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: nota-cloud
User-Agent: HTTPie/0.9.8



HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 5343
Content-Type: application/json
Date: Mon, 1 Jan 2018 00:00:00 GMT
Server: nginx/1.10.3
Strict-Transport-Security: max-age=15768000

{
    "values": [
        {
            "timestamp": 1500000000.000000,
            "value": 1.0
        },
        {
            "timestamp": 1500000001.000000,
            "value": 2.0
        },
        …
    ]
}
```

_Note:_ we did not perform any authentication
of the user, for example using an username and password,
so anyone can `POST` and `GET` data to and from the server.
This was done as a simplificiation in the lab
and is obviously bad practice.

__Do not run unauthenticated services over the public internet!__


JSON
====

In the response header we can see
a `Content-Type: application/json` line
that specifies the file type of the
response body to be JSON.

JSON is a common internet data format
based on the way JavaScript represents
data objects.

In the response above we see the main
JSON components.

- Collections of name/value pairs: `{"name_1": "value_1", "name_2: "value_2"}`
- Numbered collections: `["value_1", "value_2", "value_3"]`
- Strings: `"text"`
- Numbers: `1.0`

Names in collections are always strings, values
can be any other type, even other collections
allowing for nested structures.

How entries in a JSON file are represented
and accessed depends on the programming
language and library used but the following
pseudo-code interaction should give you an intuition:

```
> var json_file = {
>    "values": [
>        { "timestamp": 1500000000.000000, "value": 1.0 },
>        { "timestamp": 1500000001.000000, "value": 2.0 }
>    ]
> };

> json_file["values"]
[
   { "timestamp": 1500000000.000000, "value": 1.0 },
   { "timestamp": 1500000001.000000, "value": 2.0 }
]

> json_file["values"][0]
{ "timestamp": 1500000000.000000, "value": 1.0 }

> json_file["values"][0]["timestamp"]
1500000000.000000
```

[nota_cloud]: https://nota-cloud/cloud/
