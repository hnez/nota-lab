Epilogue
========

This was it, the Network-of-Things Engineering (Nota) Lab,
we hope you enjoyed it and may have even learned something.

Everything about the Nota Lab is free and
open source and you are encouraged to
participate in its development.

If you would like doing so you may meet
us on the [Nota Lab project page][www_gitlab_nota].


[www_gitlab_nota]: https://gitlab.com/hnez/nota-lab
