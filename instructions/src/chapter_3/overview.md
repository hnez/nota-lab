MQTT
====

While building IoT applications directly on top of
TCP or UDP is certainly possible the process will
be unnecessary complicated and the resulting application
will not be compatible to other applications.

One should instead use a common IoT communication protocol.
While there are a few emerging protocols for different
usecases we will concentrate on MQTT.

MQTT works on top of TCP and is based on a broker,
publishers of information and subscribers to informations.
