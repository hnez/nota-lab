Basics
======

__Congratulations__ for finishing the first task!
Yor ability to follow basic instructions will come
in handy later on.

In the course of this lab we will be using a lot of
commandline programs without a graphical user interface.
If you are not used to using a liux commandline
or want a quick refresher [the next sub-chapter](./chapter_1/bash.html)
will give you a brief introduction.

If you feel like a real `hacker` already
you may also skip the next sub-chapter and go straight
to [working with NodeMCU](./chapter_1/node_mcu.html)
and come back whenever you see fit.
