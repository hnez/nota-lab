Linux commandline basics
========================

Before we start our adventure to the depths of
textual programs we have to open a terminal emulator
as shown in the video below.

<video style="width: 100%" controls>
  <source src="assets/open_terminal.mp4" type="video/mp4">
  Applications > Terminal Emulator
</video>

Navigation
----------

The commands you execute on the commandline
always work relative to a directory on the
filesystem.
This directory is called the _working directory_
and you can find out your current working directory
by typing

```bash
pwd
```

on the commandline and hitting enter.

After hitting enter your terminal should show
something like the following:

```bash
[user@computer ~]$ pwd
/home/user
```

This means that you are currently working in the
`/home/user` directory.

Often times `$ pwd` (for "print working directory") is used as an abbreviation to saying
"Type `pwd` into a terminal emulator and hit the enter key",
the `$` prefix stems from the way the terminal displays
things, you should not type it in when asked to execute a program.

To demonstrate navigating the filesystem we first
have to create some directories to navigate to.
Execute the following commands to create the directories
`test_1` and `test_2`:

```bash
$ mkdir test_1
$ mkdir test_2
```

Now type `$ ls` (for "list") to see the content of the current
working directory. You should see, possibly among others,
your newly created test folders.

```bash
[user@computer ~]$ ls
test_1
test_2
```

To change your current working directory you use
the `cd` command:

```bash
[user@computer ~]$ pwd
/home/user
[user@computer ~]$ cd test_1
[user@computer ~/test_1]$ pwd
/home/user/test_1
```

To go up in the directory structure you can use
`..` in a path.

```bash
[user@computer ~/test_1]$ pwd
/home/user/test_1
[user@computer ~/test_1]$ cd ..
/home/user
[user@computer ~]$ cd test_2
[user@computer ~/test_2]$ pwd
/home/user/test_2
[user@computer ~/test_2]$ cd ../test_1
/home/user
[user@computer ~/test_1]$ pwd
/home/user/test_1
```

Tab completion
--------------

As typing is tedious you should, whenever possible,
use the Tab ↹ key.
When pressed once it will autocomplete whatever
you entered as far as possible, when pressed
twice it will show possible further completions.
Try it by typing the following and pressing
the tabulator key whenever a ↹ symbol comes up.

```bash
[user@computer ~/test_1]$ cd
[user@computer ~]$ cd te↹
```

(should autocomplete to `test_`)

```bash
[user@computer ~]$ cd test_↹↹
test_1/ test_2/
```

(should show options to go to)

To cancel the currently running program or to
clear the current commandline you can use
the key combination Ctrl+C (or Strg+C on a german keyboard).
Pressing these two keys asks the running program to exit.

Reading and writing files
-------------------------

We will use a text editor to create a simple file.
The text editor we use is called `nano`.
To start it type the following:

```bash
[user@computer ~]$ cd
[user@computer ~]$ cd test_1
[user@computer ~/test_1]$ nano hello.txt
```

The last command starts an interactive text editor.
For now just type `hello world!` and use the key combination
Ctrl+X (Strg+X on a german keyboard) to exit the editor.
The editor will ask if it should save the changes you made.
Type `Y` (or `J` on a german system) and hit enter to save
the changes and exit the editor.

To read the content of the file we just created we can
either use `nano` to open it or the `cat` command to
print its content to the terminal.

```bash
[user@computer ~/test_1]$ cat hello.txt
hello world!
```

_Hint:_ Be careful when `cat`ing files to the terminal,
sometimes when reading binary files instead of text files
your terminal may end up all garbled up.

Command history
---------------

In addition to using the Tab ↹ key for autocompletions
you should also use the command history.
The terminal automatically keeps a log of the last commands
you executed and you can navigate these commands using
the arrow keys ↑↓ on your keyboard.

You can also search the history using the key combination
Ctrl+R (Strg+R on a german keyboard).
