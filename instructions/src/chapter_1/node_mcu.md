NodeMCU
=======

In this lab we will be using a small microcontroller
based development board to act as our internet connected device.
These microcontrollers are most commonly programmed in
low level programming languages like C or C++, we will
instead by using an ESP8266 microcontroller running
the NodeMCU firmware, this firmware lets us program
the controller using the Lua programming language
which allows for faster experimenting than C or C++.

Communication
-------------

Before we can program the controller we have to
be able to communicate with it.
To do so please disconnect the USB connection
between your Raspberry Pi and the microcontroller board,
count to two and reconnect it.

Now run `$ dmesg` in a terminal, one of the last lines
should read something like:

```
[100000.000000] usb 1-5: cp210x converter now attached to ttyUSB0
```

the last part is what we need, the device name we use
to communicate to the microcontroller.
If dmesg shows a different device name you should
use that instead of `ttyUSB0` wherever it comes up.

Now we use `picocom` to connect to the controller,
see its output and send commands to it.

```bash
[user@computer ~]$ picocom -b 115200 /dev/ttyUSB0
picocom v3.1

[…]

Type [C-a] [C-h] to see available commands
Terminal ready
```

If you now press enter the controller should answer with
a `>` command prompt.

```bash
Type [C-a] [C-h] to see available commands
Terminal ready
>
```

To check if everything is working correctly we may
now turn on an LED on the controller board using
the following commands:

```lua
> gpio.mode(4, gpio.OUTPUT)
> gpio.write(4, gpio.LOW)
```

and turn it off again using this command:

```lua
> gpio.write(4, gpio.HIGH)
```

_Hint:_ If no LED turned on and off there may be something
wrong with your setup and you should seek assistance.

The controller board has an onboard LED connected
between the supply voltage and pin 4 of the microcontroller.
For more information on the commands you just executed consult
the [NodeMCU documentation][nodemcu_doc_gpio].

Uploading code
--------------

Using the NodeMCU commandline for anything but basic
commands quickly becomes tedious, so the next step is to
upload full programs to the controller in one go.

To do so we first have to exit picocom.

__Always remember to exit picocom before uploading a program.__

Doing so can be a bit tricky as the process consists of two
key combinations. You have to first press Ctrl+A and then Ctrl+X
(Strg+A and then Strg+X on a german keyboard, respectively).

After you hit these key combinations picocom should exit with
the output:

```bash
>
Terminating...
Thanks for using picocom
[user@computer ~]$
```

Before we can upload a project to the microcontroller we
need a project to upload, a basic template is located in
the `/usr/src/nodemcu_base` directory of your Raspberry Pis.

Copy this template to a new project directory using the following
commands:

```bash
[user@computer ~]$ mkdir projects
[user@computer ~]$ cp -rv /usr/src/nodemcu_base projects/chapter_1
[user@computer ~]$ cd projects/chapter_1
```

The template contains three files `init.lua` `credentials.lua`
`application.lua` before going into any detail on what the
purpose of these files is we can just upload them as-is using
the following command (remember that you might have to replace the
`ttyUSB0` part):

```bash
[user@computer ~]$ nodemcu-uploader --port /dev/ttyUSB0 upload *.lua
opening port /dev/ttyUSB0 with 115200 baud
Preparing esp for transfer.
Transferring init.lua as init.lua
Transferring credentials.lua as credentials.lua
Transferring application.lua as application.lua
All done!
[user@computer ~]$
```

_Hint:_ If the command above throws any errors you should try
pressing the reset button on the microcontroller board and quickly
retry flashing after releasing it.

After uploading you can reconnect picocom, press the reset button
on the microcontroller board and should see output like the following:

```
[user@computer ~]$ picocom -b 115200 /dev/ttyUSB0
…
init: WiFi credentials are not configured
init: startup will continue in 3s
> init: handing over to application
Hello from your NodeMCU application!
Hi from your NodeMCU application!
…
```

NodeMCU project structure
-------------------------

The template project is split into the files `init.lua`, `credentials.lua` and
`application.lua`, we will now take a look at these files in a text editor.

<video style="width: 100%" controls>
  <source src="assets/open_text_editor.mp4" type="video/mp4">
  Applications > Acessories > Text Editor
</video>

### init.lua

This file is executed by the firmware on the microcontroller,
it is used to set up a WiFi connection and hand over to `application.lua`.

### credentials.lua

This file contains name and password of the WiFi network to connect to.

### application.lua

This file contains the application running on the microcontroller,
most of your code goes in here.

_Task:_ Read the code in `application.lua` and try editing the `greeting_print`
function so that the LED is turned on whenever the application prints
"Hello from your NodeMCU application!" and off whenever the application prints
"Hi from your NodeMCU application!".

[nodemcu_doc_gpio]: https://nodemcu.readthedocs.io/en/master/en/modules/gpio/
