Networking
==========

In this chapter we will work with the
[netcat][wiki_netcat] commandline program,
NodeMCU and the fundamental internet protocols
TCP and UDP to facilitate communication
between the computer and the microcontroller.

[wiki_netcat]: https://en.wikipedia.org/wiki/Netcat
