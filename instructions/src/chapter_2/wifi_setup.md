WiFi Setup
==========

Before we can start sending IP packets to
and from our microcontroller board we
have to connect it to an IP network.

For this we will be using WiFi.
If you, for example by using your phone,
scan for WiFi networks near the lab
you should find a few networks with names
starting with `nota_`.
These networks are spanned by the Raspberry Pis
on your desks.

To find out which of these networks belongs
to your Pi you can execute the following command:

```bash
[user@computer ~]$ cat /etc/hostapd/hostapd.conf
interface=wint0
driver=nl80211
channel=1
wpa=2
ssid=nota_x
wpa_passphrase=password_x
```

Of particular interest are the last two lines
containing the name of the WiFi network
and its password.

---

Next we will create a new NodeMCU project for this
chapter and use the WiFi credentials to connect to
the network.

First create the new project:

```bash
[user@computer ~]$ cd projects
[user@computer ~]$ cp -rv /usr/src/nodemcu_base projects/chapter_2a
[user@computer ~]$ cd projects/chapter_2a
```

Then use the text editor to open the `credentials.lua`
file in your new project.
After uncommenting the two lines in this file
and entering the WiFi name and Password
the file should look something like this:

```bash
[user@computer ~]$ cat credentials.lua
SSID= "nota_x"
PASSWORD= "password_x"
```

We can now upload the project to the project to
the microcontroller like before:

```bash
[user@computer ~]$ nodemcu-uploader --port /dev/ttyUSB0 upload *.lua
```

When you now attach to the output of the microcontroller
using picocom and press the reset button you should
get messages like the below, showing that connecting
to the WiFi network was sucessful.

```bash
[user@computer ~]$ picocom -b 115200 /dev/ttyUSB0
init: connecting to AP: nota_1
init: retrying connection 2/75
init: connected to AP: nota_1
init: got IP address 192.168.94.158 via DHCP
init: startup will continue in 3s
init: handing over to application
Hello from your NodeMCU application!
Hi from your NodeMCU application!
```
