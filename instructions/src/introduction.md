Introduction
============

Welcome to the _Network-of-Things Applications (NoT A) Lab_.

This lab will guide you through the components used to connect
devices to the internet and will give you a hands-on experience
using actual hardware and software as well as realistic
network topologies.

We will cover topics like software development on
ressource-constrained embedded devices,
network protocols used in the IoT,
local data processing in a so-called fog zone
and cloud-based data processing.

But before we dive right in we will have to cover
some basics to get you started.

As a first task click on the arrow to the right
to get to the first chapter.
