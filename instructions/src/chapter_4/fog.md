Fog computing scenario
======================

In this subchapter we will move the
processing from our sensor and actuator nodes
to the Raspberry Pi, acting as our fog node.

To do so we will use custom functions, written
in javascipt, in Node-RED.

The Video below gives you an overview of how
a function node is added to Node-RED.

<video style="width: 100%" controls>
  <source src="assets/nodered_function.mp4" type="video/mp4">
  Add a custom function to Node-RED
</video>

This is the code snipped pasted into
the function block 45 seconds into the video:

```javascript
var led_status= context.get('led_status');

if (led_status === undefined) {
    led_status = 'off';
}

if (msg.payload === 'toggle') {
    if (led_status === 'on') {
        led_status = 'off';
    }
    else {
        led_status = 'on';
    }
}

context.set('led_status', led_status);

var out = {'payload': led_status};

return out;
```

_Task:_ Recreate the flow shown in the video and, by reading
the sourcecode and analyzing the behaviour, find out what the
function node does.

_Task:_ Change the function block so that, in addition to the
toggle functionality, injecting `on` and `off` messages
sets the LED state to `on` and `off`, regardless of previous state.

---

Now we want to integrate the function block into our hardware setup.

_Task:_ Edit the program on your publishing microcontroller to
send `pressed` to the `/button` topic whenever the button
input goes from unpressed to pressed.

_Task:_ Edit the function block in Node-RED to toggle the
LED state when the string `pressed` is received instead of
`toggle`.

_Task:_ Add MQTT subscription and publishing nodes to
the flow so that pressing the button on the publishing
microcontroller toggles the LED on the subscribing microcontroller.

_Task:_ Add a delay node to delay the reaction between pressing
the button an changing the LED by two seconds.

---

__TODO:__ Do more complicated stuff with, for example, delays or triggers.

__TODO:__ Change publisher to read another sensor, like ultrasonic distance
or temperature and publish measurements in a generic format like cm or °C.

__TODO:__ Change subscriber to control another actuator than the LED
and take commands in a generic format.
