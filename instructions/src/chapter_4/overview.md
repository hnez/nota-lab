Fog computing
=============

In this chapter we will make our microcontroller board
dumb devices that do one thing and do that one thing
well like sending sensor data to the MQTT broker
or taking messages from the broker and controlling
actuators.

We will introduce a new component in our network
that does the computing required to link the
dumb devices.

For this we will be using [Node-RED][www_nodered].

[www_nodered]: https://nodered.org/
