Node-RED
========

Node-RED provides "Flow-based programming for the Internet of Things",
we will find out what that means by diving right in.

Node-RED is already installed on your Raspberry Pis, so all you
need to do is start it on the commandline:

```bash
[user@computer ~]$ node-red
Welcome to Node-RED
===================
…
1 Jan 00:00:09 - [info] Starting flows
1 Jan 00:00:09 - [info] Started flows
1 Jan 00:00:09 - [info] Server now running at http://127.0.0.1:1880/
```

You can now open the Node-RED interface by right-clicking
on the URL in the last line and selecting "Open Link".

---

The video below shows how to setup your
first Node-RED flow:

<video style="width: 100%" controls>
  <source src="assets/nodered_easyflow.mp4" type="video/mp4">
  Setting up a simple Node-RED flow
</video>

_Task:_ Try replicating the steps seen in the video.

---

So what did just happen there?

First we start with the blank Node-RED interface.

![Node-RED interface](assets/nodered_start.png)

Then we drag the "inject" and "debug "nodes from the pool
of available nodes into our flow window.

![Node-RED inject inject](assets/nodered_place_inject.png)

Next we connect the Nodes. That means whenever the
"inject" node produces output it will go into
the input of the "debug" node.

![Node-RED connect nodes](assets/nodered_connect.png)

Next we start the flow…

![Node-RED deploy](assets/nodered_deploy.png)

… open the debug output…

![Node-RED debug output](assets/nodered_debug.png)

… and trigger the inject node.

![Node-RED](assets/nodered_inject.png)

The inject node sends, via the connection we made,
a message to the debug node and the debug node prints
that message to the debug output window.
