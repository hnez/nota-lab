Node-RED and MQTT
=================

The video below shows you how to connect Node-RED
to the MQTT broker running on the Raspberry Pi:

<video style="width: 100%" controls>
  <source src="assets/nodered_mqtt.mp4" type="video/mp4">
  Add an MQTT Broker to Node-RED
</video>

_Task:_ Try replicating the steps seen in the video.

---

Okay, once again: what did just happen there?

First we start with an empty flow and add
an MQTT subscription node.

![Add Subscribe Node](assets/nodered_mqtt_subscribe.png)

Next we set up the MQTT broker configuration
to connect to the broker running on `localhost:1883`.

![Configure broker](assets/nodered_mqtt_broker.png)

Then we configure the node to subscribe to the `/led`
topic.

![Subscribe to led topic](assets/nodered_mqtt_topic.png)

Next we set up an MQTT publish node to use the same
broker and to publish to the `/led` topic.

![Publish to led topic](assets/nodered_mqtt_publish.png)

Finally we add a debug output node and an
injection node.

![Add debug nodes](assets/nodered_mqtt_inject.png)

The inject node will, by default, inject a timestamp
message, we configure it to instead publish the
string `on` whenever it is triggered.

![Inject "on"](assets/nodered_mqtt_inject_string.png)

The image below shows the flow in action:
the injected message is sent to the broker,
received by the subscription node and displayed
by the debug node.

![Run MQTT flow](assets/nodered_mqtt_run.png)

---

Now that we know how to use Node-RED with
MQTT we can make it actually _do_ something.

_Task:_ Add an injection node that injects the
string `off`.

_Task:_ Connect the microcontrollers that you
programmed in the last chapter and controll
the LEDs from Node-RED.

---

Later on we want to make our setup more
flexible by routing all the messages through
Node-RED.

Right now we will prepare that step by
decoupling the functionality of the
publishing microcontroller and the
subscribing microcontroller.

_Task:_ Edit the program on the publishing
microcontroller to publish to the topic
`/button` instead of the topic `/led`.

The publishing microcontroller should now
no longer be able to controll the LED.

_Task:_ In Node-RED: subscribe to the `/button`
topic and, using a debug node, make sure that
messages are correctly received there.

_Task:_ In Node-RED: forward messages from the
`/button` topic to the `/led` topic so that
the publishing microcontroller can
control the LED again.
