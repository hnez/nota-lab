steps:
  - mkimg: "{{ output }}"
    size: 7G

  - mklabel: msdos
    device: "{{ output }}"

  - mkpart: primary
    device: "{{ output }}"
    start: 0%
    end: 100%
    part-tag: root-part

  - mkfs: ext4
    partition: root-part
    label: vmroot

  - mount: root-part
    fs-tag: root-fs

  - unpack-rootfs: root-fs

  - debootstrap: stretch
    mirror: http://deb.debian.org/debian
    target: root-fs
    components:
    - main
    - contrib
    - non-free
    unless: rootfs_unpacked

  - chroot: root-fs
    shell: |
      apt update
    unless: rootfs_unpacked

  - apt: install
    packages:
    - python3
    - python3-bottle
    - python3-sqlobject
    - python3-gevent
    - gunicorn3
    - nginx
    - ssh
    - sudo
    - locales
    - console-keymaps
    - curl
    - httpie
    - rsync
    - libnss-resolve
    - libnss-myhostname
    - linux-image-amd64
    - console-setup
    fs-tag: root-fs
    unless: rootfs_unpacked

  - cache-rootfs: root-fs
    unless: rootfs_unpacked

  - chroot: root-fs
    shell: |
      adduser --disabled-password --gecos "" tutor
      usermod -a -G sudo tutor
      usermod -a -G www-data tutor
    root-fs: root-fs

  - shell: |
      install -m 644 -o root -g root assets/vm/50-eth0.network "${ROOT?}/etc/systemd/network/50-eth0.network"

      install -m 644 -o root -g root certificates/nota-ca.crt "${ROOT?}/usr/local/share/ca-certificates/"

      install -o root -g root -d "${ROOT?}/etc/nginx/includes"
      install -m 644 -o root -g root assets/vm/nginx-site.conf "${ROOT?}/etc/nginx/sites-available/default"
      install -m 644 -o root -g root assets/vm/tls.conf "${ROOT?}/etc/nginx/includes/tls.conf"
      install -m 644 -o root -g root certificates/nota-cloud.bundle.crt "${ROOT?}/etc/nginx/nota-local.crt"
      install -m 600 -o root -g root certificates/nota-cloud.key "${ROOT?}/etc/nginx/nota-local.key"

      install -m 755 -o root -g root assets/vm/nota_initial_setup "${ROOT?}/usr/local/sbin/nota_initial_setup"
      install -m 644 -o root -g root assets/vm/nota_initial_setup.service "${ROOT?}/etc/systemd/system/nota_initial_setup.service"

      install -o www-data -g www-data -d "${ROOT?}/var/www/nota-nossl/"
      install -m 644 -o www-data -g www-data certificates/nota-ca.crt "${ROOT?}/var/www/nota-nossl/"
      install -m 644 -o www-data -g www-data assets/vm/index-nossl.html "${ROOT?}/var/www/nota-nossl/index.html"

      mkdir "${ROOT?}/var/www/nota/"
      cp assets/vm/index.html "${ROOT?}/var/www/nota/"
      cp -R ../bootstrap/book "${ROOT?}/var/www/nota/nota-meta"
      cp -R ../instructions/book "${ROOT?}/var/www/nota/nota"
      cp -R assets/vm/cloud_server/ "${ROOT?}/var/www/nota/cloud"
      chown -R tutor:www-data "${ROOT?}/var/www/"

      install -o 1000 -g 1000 -d "${ROOT?}/home/tutor/.config/systemd/user/default.target.wants"
      install -o 1000 -g 1000 -d "${ROOT?}/home/tutor/api/"

      install -m 644 -o 1000 -g 1000 assets/vm/cloud_server/api.py "${ROOT?}/home/tutor/api/api.py"
      install -m 644 -o 1000 -g 1000 assets/vm/nota_api.service "${ROOT?}/home/tutor/.config/systemd/user/nota_api.service"
      ln -s "${ROOT?}/home/tutor/.config/systemd/user/nota_api.service" "${ROOT?}/home/tutor/.config/systemd/user/default.target.wants/nota_api.service"
    root-fs: root-fs

  - chroot: root-fs
    shell: |
      update-ca-certificates

      systemctl --no-reload enable systemd-networkd
      systemctl --no-reload enable systemd-resolved
      systemctl --no-reload enable nota_initial_setup

  - grub: bios
    root-fs: root-fs
    root-part: root-part

  - chroot: root-fs
    shell: |
      ln -sf /var/run/systemd/resolve/resolv.conf /etc/resolv.conf

      apt-get clean
      rm -rf /var/lib/apt/lists
      rm -f /etc/machine-id /etc/localtime /etc/hostname /etc/locale.conf
      rm -f /etc/ssh/ssh_host_*_key*
