#!/bin/bash

umask 022
sudo env -i LC_CTYPE=C.UTF-8 PATH="/usr/sbin:/sbin:$PATH" \
    vmdb2 "${@}"
