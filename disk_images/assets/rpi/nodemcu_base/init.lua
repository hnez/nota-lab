dofile("credentials.lua")

function application_start()
   if file.open("application.lua") == nil then
      print("init: application.lua not found")
   else
      print("init: handing over to application")
      file.close("application.lua")

      dofile("application.lua")
   end
end

function wifi_connect_event(ev)
   print("init: connected to AP: "..ev.SSID)
   if disconnect_ct ~= nil then disconnect_ct = nil end
end

function wifi_got_ip_event(ev)
   print("init: got IP address "..ev.IP.." via DHCP")
   print("init: startup will continue in 3s")

   tmr.create():alarm(3000, tmr.ALARM_SINGLE, application_start)
end

function wifi_disconnect_event(ev)
   if ev.reason == wifi.eventmon.reason.ASSOC_LEAVE then
      --the station has disassociated from a previously connected AP
      return
   end

   -- total_tries: how many times the station will attempt to connect to the AP. Should consider AP reboot duration.
   local total_tries = 75
   print("\ninit: disconnected from AP: "..ev.SSID)

   --There are many possible disconnect reasons, the following iterates through
   --the list and returns the string corresponding to the disconnect reason.
   for key,val in pairs(wifi.eventmon.reason) do
      if val == ev.reason then
         print("init: disconnect due to: "..val.."("..key..")")
         break
      end
   end

   if disconnect_ct == nil then
      disconnect_ct = 1
   else
      disconnect_ct = disconnect_ct + 1
   end

   if disconnect_ct < total_tries then
      print("init: retrying connection "..(disconnect_ct+1).."/"..total_tries)
   else
      wifi.sta.disconnect()
      print("init: giving up the retries")
      disconnect_ct = nil
   end
end

function wifi_setup()
   -- Register WiFi Station event callbacks
   wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
   wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
   wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)

   print("init: connecting to AP: "..SSID)
   wifi.setmode(wifi.STATION)
   wifi.sta.config({ssid=SSID, pwd=PASSWORD})
end

if (SSID == nil) or (PASSWORD == nil) then
   print("init: WiFi credentials are not configured")
   print("init: startup will continue in 3s")
   tmr.create():alarm(3000, tmr.ALARM_SINGLE, application_start)
else
   wifi_setup()
end
