function greeting_print()
   if greeting~="Hello" then
      greeting= "Hello"
   else
      greeting= "Hi"
   end

   print(greeting.." from your NodeMCU application!")
end

function greeting_setup()
   gpio.mode(4, gpio.OUTPUT)
   gpio.write(4, gpio.LOW)

   -- schedule greeting_print() to run once a second
   tmr.create():alarm(1000, tmr.ALARM_AUTO, greeting_print)
end

greeting_setup()
