#!/usr/bin/env python3

import bottle as bo
import datetime as dt
import itertools as it

import json

from gevent import queue

class WebApi(bo.Bottle):
    def __init__(self):
        super().__init__()

        self.post('/api/log/<key>', callback=self.api_new_value)
        self.get('/api/log', callback=self.api_get_keys)
        self.get('/api/log/<key>', callback=self.api_get_values)
        self.get('/api/stream', callback=self.api_get_stream)

        self.get('/', callback=self.serve_static('index.html', 'text/html'))
        self.get('/cloud_app.css', callback=self.serve_static('cloud_app.css', 'text/css'))
        self.get('/cloud_app.js', callback=self.serve_static('cloud_app.js', 'text/javascript'))
        self.get('/chart.js', callback=self.serve_static('chart.js', 'text/javascript'))

        self.streams = list()
        self.storage = dict()

    def serve_static(self, path, mime):
        def req():
            bo.response.set_header('Content-Type', mime)

            return open(path)

        return req

    def api_get_stream(self):
        bo.response.set_header('Content-Type', 'text/event-stream')
        bo.response.set_header('Cache-Control', 'no-cache')
        bo.response.set_header('X-Accel-Buffering', 'no')
        bo.response.set_header('Access-Control-Allow-Origin', '*')

        history = iter(
            (timestamp, key, value)
            for (key, elems) in self.storage.items()
            for (value, timestamp) in elems
        )

        history = sorted(history, key= lambda a: a[0])
        stream = queue.Queue(32)

        self.streams.append(stream)

        for (timestamp, key, value) in it.chain(history, stream):
            data = {
                'timestamp' : timestamp.timestamp(),
                'key' : key,
                'value' : value,
            }

            yield 'data: {}\n\n'.format(json.dumps(data))

    def api_new_value(self, key):
        if bo.request.json is None:
            bo.abort(415, 'request not json encoded')

        req= bo.request.json

        if 'value' not in req:
            bo.abort(400, 'value field missing')

        value= float(req['value'])
        timestamp= dt.datetime.now()

        if key not in self.storage:
            self.storage[key]= list()

        self.storage[key] = self.storage[key][-99:] + [(value, timestamp)]

        for stream in self.streams:
            try:
                stream.put_nowait((timestamp, key, value))
            except queue.Full:
                self.streams.remove(stream)

        bo.response.status = 201

    def api_get_keys(self):
        keys = list(self.storage.keys())

        bo.response.set_header('Access-Control-Allow-Origin', '*')

        return({'keys': keys})

    def api_get_values(self, key):
        if key not in self.storage:
            bo.abort(404)

        values = list(
            {'timestamp' : timestamp.timestamp(), 'value' : value}
            for value, timestamp in self.storage[key]
        )

        bo.response.set_header('Access-Control-Allow-Origin', '*')

        return({'values' : values})

app = WebApi()

if __name__ == '__main__':
    app.run(host='localhost', port=8080, server='gevent')
