"use strict";

var charts = {};


function on_page_load() {
  function create_chart(label) {
    var block = document.createElement('div')
    block.classList.add('sensor');

    var heading = document.createElement('h3')
    heading.appendChild(document.createTextNode('Fog Node ' + label + ':'));
    block.appendChild(heading);

    var canvas = document.createElement('canvas');
    canvas.setAttribute('width', '100%');
    canvas.setAttribute('height', '40em');

    var canvas_pad = document.createElement('div');
    canvas_pad.appendChild(canvas);
    canvas_pad.classList.add('svgpad');
    block.appendChild(canvas_pad);

    var anchor = document.getElementById('sensor_overview');
    anchor.appendChild(block);

    var chart_cfg = {
      type: 'line',
      data: {
	labels: [],
	datasets: [{
          data: [],
          fill: true,
          backgroundColor: '#141D2033',
	  borderColor: '#141D20',
        }]
      },
      options: {
	responsive: true,
	title: { display: false },
        legend: { display: false },
	tooltips: { mode: 'index', intersect: false },
	hover: { mode: 'nearest', intersect: true },
	scales: {
	  xAxes: [{
	    display: true,
	    ticks: { display: false }
	  }],
	  yAxes: [{
	    display: true,
	    scaleLabel: { display: false },
	  }]
	}
      }
    };

    var chart = new Chart(canvas, chart_cfg);

    return chart;
  }

  function on_stream_message(event) {
    var ev_data = JSON.parse(event.data);

    if (!(ev_data.key in charts)) {
      charts[ev_data.key] = create_chart(ev_data.key);
    }

    var chart = charts[ev_data.key];
    var labels = chart.data.labels;
    var data = chart.data.datasets[0].data;

    labels.push(ev_data.timestamp);
    data.push(ev_data.value);

    chart.update();

    while (data.length > 40) {
      data.shift();
    }

    while (labels.length > 40) {
      labels.shift();
    }
  }

  var stream = new EventSource("/api/stream");
  stream.onmessage = on_stream_message;
}
