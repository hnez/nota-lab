_WARNING!_ This fork is, at the moment, not actively maintained
and may contain errors. This should not stop you from using it
as a base for your own lab.

NoT A Lab
=========

![Lab Logo](diagrams/logo.png)

NoT A Lab is teaching lab for Network of Things applications.
NoT A Lab is a fork of the NoTE-Lab offered at the TU Braunschweig
and is developed independently.
This repository contains the necessary materials to teach yourself
or a course about programming ESP8266 microcontrollers using [NodeMCU][www_nodemcu],
MQTT as an IoT protocol, Node-RED to tie everything together and
how to use REST over HTTPS to communicate with the cloud.

If you are interested in running
your own instance of this lab you
should have a look at the bootstrapping
instructions in the
[`bootstrap/src` folder](bootstrap/src/chapter_1/mdbook.md).

You can also go and find rendered versions
of the bootstrapping instructions [here][www_nota_meta]
and of the lab instructions [here][www_nota].

Lab Setup
=========

Overview
--------

The following diagrams summarize the hardware and software
layout of the lab.

A group of two to three students is assigned one Raspberry Pi
and two ESP8266 based development boards, one of which is connected
to a sensor like a thermometer while the other is connected to
an actuator like a fan.

The Raspberry Pis serve two purposes:

- Acting as a workstation for the students to program
  the ESP boards on.
- Running the MQTT broker and Node-RED event processing software.

The layout is designed in a way that any group
runs their own isolated fog zone containing a local
MQTT broker and processing software.
All the groups share an additional centralized Raspberry Pi
simulating a cloud environment.
This cloud environment is used to store and display
aggregated sensor data that is forwarded by the fog nodes.

Hardware
--------

![HW Setup](diagrams/lab_setup_hw.png)

The diagram above shows the per-group and common
hardware components of the lab.

Each Group is provided two ESP boards
connected to an, also provided, Raspberry Pi using USB.
These USB connections are used for programming the
ESPs, outputting debug information and to supply power.

Once programmed, the ESPs connect to a WiFi
network provided by the Raspberry Pi and are
able to communicate using IP protocols.

The student Raspberry Pis are connected to a
common network using ethernet in order to be
able to connect to the also common "cloud" environment.

Software
--------

![SW Setup](diagrams/lab_setup_sw.png)

The diagram above shows the per group and common
software setup with the associated protocols
while abstracting away most of the hardware components.

The student groups' RPis are set up to act as an
WiFI access point using [hostapd][wiki_hostapd].

The Raspberry Pis also run an MQTT broker
for message routing between MQTT clients ([mosquitto][www_mosquitto]).

The ESPs connect to this MQTT broker via the WiFi network.

To implement the logic of "what should happen when"
the Raspberry Pis also run [NodeRed][www_nodered],
where dataflows can be configured graphically and
using javascript.
Enabling tasks like "Turn on fan if temperature is above 40°C"
by subscribing to a `/temperature` MQTT topic, performing the comparison
and publishing to a `/fan_state` MQTT topic.

NodeRed can then be used to communicate with a centralized
cloud infrastructure using HTTPS+REST for data aggregation or
remote controlling reasons.

This cloud infrastructure is simulated on an
additional RPi.

Lab Content
===========

Unit 1 - NodeMCU & Linux basics
-------------------------------

In this unit the students should gain experience with
the working environment of the lab.

This means:

- Using the Linux commandline on the assigned Raspberry Pis
  to connect to their microcontrollers via USB
- Using the NodeMCU commandline to directly execute
  simple commands on the microcontrollers
- Learning the basics of the Lua programming language
  used by NodeMCU
- Using a text editor to write larger programs for the
  microcontroller
- Uploading the programs to the microcontroller
- Interfacing to the sensors and actuators connected
  to their microcontrollers

Unit 2 - Network connecting NodeMCU
-----------------------------------

In this unit the students should learn how to
connect their microcontrollers to a network.

To do so they should:

- Connect NodeMCU to the WiFi network spanned by their
  Raspberry Pi
- Find the IP of the microcontroller and `ping`
  it from the Raspberry Pi
- Upload a program that acts as a TCP server
  and echos back its input. Connect to it using
  `netcat`
- Upload a program that acts as a TCP client and
  connect it to a `netcat` server.

Unit 3 - MQTT connecting NodeMCU
--------------------------------

In this unit the students should learn about the
broker based publish/subscribe struture of MQTT
and how to use it to connect devices.

To do so they should:

- Prepare at home by reading provided materials
  regarding MQTT
- Run the mosquitto MQTT broker on their Raspberry Pis
- Connect their microcontrollers to the MQTT broker
  and observe the debug outputs to check for a sucessful connection
- Program the microcontroller with attached sensor to publish
  the sensor output to a MQTT topic
- Program the actuator microcontroller to subscribe to the
  same topic and perform actions on the sensor data

A group of students with a temperature sensor attached to one microcontroller
and a fan attached to the other microcontroller should,
after completing this unit, have a setup where the fan turns
on when the temeperature reaches a given threshold.

Another group may turn on a lamp when a light sensor
detects darkness.

Unit 4 - Local data processing (Fog computing)
----------------------------------------------

In this unit the should learn about the benefits
of decoupling the aquisition, processing and usage of
sensor data from the devices generating and using
this data.

To demonstrate this concept [Node-RED][www_nodered]
is used, a framework that allows programming event processing
steps using a flow based approach.

In this unit the students should:

- Edit the program on their sensor node to publish
  the sensor data in a generally useful format not
  targeted for a special application.

  A temperature sensor may publish a temperature in
  degree celsius or kelvin on a `/sensor/temperature` topic.
  A light sensor may publish a value between
  0 and 100, where 100 is very bright light,
  on a `/sensor/brightness` topic.

- Edit the program on their actuator node to subscribe
  to a topic different from the sensor node that
  is not specific to the task both nodes should perform.

  For a fan node this may be a `/actuator/fan_speed` topic that
  sets the fan speed on a scale from 0 to 100.
  A lamp node may subscribe to a `/actuator/light_power` topic
  that sets the lamps brightness on a scale from 0 to 100.

- Start Node-RED and create a flow that prints the output
  from the sensor node to the debug output

- Add blocks to the flow to publish fixed values to
  the actuator node

- Write a custom function that takes the published sensor data
  and sets the actuator based on certain rules

Unit 5 - Remote data processing (Cloud computing)
-------------------------------------------------

In this unit the students should learn about the concepts
of fog to cloud.

To do so they should:

- Add blocks to their Node-RED flow to forward
  data to the "cloud" server using HTTPS+REST
- Request data from the "cloud" and use it in their
  fog zone.

[www_nota_meta]: https://tut.zerforscht.de/nota-meta/
[www_nota]: https://tut.zerforscht.de/nota/
[www_nodemcu]: http://nodemcu.com/index_en.html
[www_nodered]: https://nodered.org/
[www_mosquitto]: https://mosquitto.org/

[wiki_hostapd]: https://en.wikipedia.org/wiki/Hostapd
